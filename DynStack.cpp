/*Copyright 2019 J.P.Reilly
 *
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 */


#include "Arduino.h"
#include "DynStack.h"

//constructor
DynStack::DynStack() {
    //makes sure an empty stack points to nullptr
	Bottom = nullptr;
    
    //initalize node counter
    NodeCount = 0;
}

//destructor
DynStack::~DynStack() {
	//pop each node until stack is empty
	while (!empty()) {
		pop();
	}
}

//add node
void DynStack::push(String Value) {
	//add new node as bottom, push old bottom up the stack by pointing new node ->next to old bottom
	Bottom = new StackNode(Value, Bottom);
    
    //+1 node
    NodeCount += 1;
}

//check for empty stack 
bool DynStack::empty() const {
    //if bottom point is nullptr, stack is empty, return true...
    if (!Bottom) {
        return true;
    }
    
    //...else bottom pointer points to a node, stack is not empty return false
    return false;
}

//remove node, modified 4/25/19 to accept # of nodes to pop, default is 1
String DynStack::pop(int Pops) {
    //holds the node to pop (delete)
	StackNode *TempNode;
    //holds the value from the deleted node to be returned
	String Value;
    
    //will pop multiply nodes if Pops is a number > 1, only returns the last nodes value
    while(Pops){
        //if stack is empty return empty string
        if (empty()) {
            Value = "";
        }
        else {
            //return bottom node value
            Value = Bottom->Value;
            
            //dead man walking node pointer
            TempNode = Bottom;
            
            //point bottom to next node aka the new bottom node
            Bottom = Bottom->Next;
            
            //delete old bottom
            delete TempNode;
            
            //-1 node
            NodeCount -= 1;
        }
        
        Pops--;
    }
    
	return Value;
}

//return pointer to bottom value without popping, or will be nullptr if stack is empty
String* DynStack::bottom() const { 
	if(Bottom){
        return &Bottom->Value;
    }
    else {
        return nullptr;
    }
}

//return length of stack, added 4/28/19
int DynStack::length() const {
    return NodeCount;
}

//return pointer to given node in the stack, added 4/18/19
String* DynStack::node(int NodeNumber) const {
    //tracks the current node starting at the first
    StackNode *TempNode = Bottom;
    
    //work through the nodes to nodenumber or return null
    for(int i = 0; i < NodeNumber;i++){
        //checks pointer to current node, if nullptr ->next is found before node # thats needed, reutrn nullptr
        if(TempNode){
            TempNode = TempNode->Next;
        }
        else {
            return nullptr;
        }
        
    }
    
    //return pointer to the found nodes value
    return &TempNode->Value;
}

void DynStack::insert(String Value, int StackPos){
    //tracks the current node which will become the ->next after insertion
    StackNode *CurrentNode = Bottom;
    
    //tracks the node before the current node
    StackNode *PreviousNode = nullptr;
    
    //used for the insertion node
    StackNode *NewNode;
    
    //work through the nodes to the node after the insertion position or the end of the stack
    //stack is 0 based, stackpos is 1 based
    for(int i = 0; i < StackPos;i++){
        if(CurrentNode){
            PreviousNode = CurrentNode;
            CurrentNode = CurrentNode->Next; 
        }
    }
    
    //insert new node, linking to both the previous and next nodes. 
    //node will be insered at stackpos or end of stack if stackpos > stack length
    //if previous node is nullptr still, stack is emtpy, push a new node
    if(PreviousNode){
        NewNode = new StackNode(Value, CurrentNode);
        PreviousNode->Next = NewNode;
    }
    else {
        push(Value);
    }
    
    //+1 node
    NodeCount += 1;
}

