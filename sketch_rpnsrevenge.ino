/*Copyright 2019 J.P.Reilly
 *
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 */

// Final Project
// Joseph Reilly
// April 2019
// -----------------------------------------------------------------------------------------------------
// RPN's Revenge, Arduino based RPN calculator featuring a 20x4 display and 4x4 analog keypad
// -----------------------------------------------------------------------------------------------------

//------Libraries------
//dynamic stack class, LCD Library
#include <DynStack.h>
#include <LiquidCrystal.h>

//------Global Constants------
//lcd control/data lines
const int RS = 7, E = 8, D4 = 9, D5 = 10, D6 = 11, D7 = 12;
const int DEC_P = 3;

//------Global Variables------
LiquidCrystal lcd(RS, E, D4, D5, D6, D7); //2004a lcd display object
char KeyValue = NULL; //key identified as being pressed by InKey() and DoMath()
DynStack RpnStack; //dynamic stack object used for storage
bool InputMode = false; //Input mode flag used by DoMath() and UpdateDisplay()
String InputPrompt = ""; //holds input value between calls




void setup(){
    //debug code
    //Serial.begin(9600);
  
    //keypad input via analog
    pinMode(A0, INPUT); 
    
    //setup and clear 2004a lcd
    lcd.begin(20,4);
    lcd.clear();
  
    //vanity screen
    lcd.setCursor(1,1);
    lcd.print("RPN's Revenge v0.1");
    lcd.setCursor(1,2);
    lcd.print("---Reilly, J.P.---");
    delay(1500);
    
    UpdateDisplay();
}

void loop(){

    
    //activity on A0? call InKey to check the keypad input
    //test against 200 because the dispaly makes noise on the power lines that A0 picks up
    if (analogRead(A0) > 300){
        InKey();
        DoMath();
        UpdateDisplay();
    }
    
    
}

//(dependencies) globals InputMode, KeyValue
void InKey(){
    //reset identified key
    KeyValue = NULL;
    
    //raw key input, priming read
    int KeyRawState = analogRead(A0);
    
    //holds highest key input from raw key state
    int KeyInput = 0;
    
    //flag if held down key (alt key) is detected
    bool AltKey = false; 
    int AltKeyCounter = 0;
    
        
    //capture activity/keypress until user releases key
    //this gets around the slope of analog values the key input goes through when pressed
    //tested against 300 because the dispaly makes noise on the power lines that A0 picks up
    while(KeyRawState > 300){
        //retain highest value seen from analog input (keypad)
        if(KeyRawState > KeyInput){
            KeyInput = KeyRawState;
        }
        
        AltKeyCounter++;
        KeyRawState = analogRead(A0);
    }
    
    //if a key is held for 2 seconds flag for alternate functions
    if(AltKeyCounter > 2000){
        AltKey = true;
    }    
        
    //if the processed key input is greater than 300, its a real key press, process the key press
    if(KeyInput > 300){
        if(KeyInput > 340 && KeyInput < 360){
            if(AltKey){
                KeyValue = 'L'; //D key recover last x
            }
            else{
                KeyValue = '+'; //D key
            }
        }
        else if(KeyInput > 360 && KeyInput < 372){
            if(AltKey){
                KeyValue = 'D'; //# key, drop/backspace
            }
            else{
                KeyValue = 'E'; //# key
            }
        }
        else if(KeyInput > 372 && KeyInput < 385){
            if(AltKey){
                KeyValue = 'R'; //roll stack
            }
            else{
                KeyValue = '0';
            }
        }
        else if(KeyInput > 388 && KeyInput < 400){
            if(AltKey){
                KeyValue = 'W'; //* key, swap x and y
            }
            else{
                KeyValue = '.'; //* key
            }
        }
        else if(KeyInput > 407 && KeyInput < 425){
            if(AltKey){
                KeyValue = 'N'; //C key, negate number
            }
            else{
                KeyValue = '-'; //C key
            }
        }
        else if(KeyInput > 422 && KeyInput < 440){
            if(AltKey){
                KeyValue = 'X'; //raise y to power of x
            }
            else {
                KeyValue = '9';
            }
        }
        else if(KeyInput > 442 && KeyInput < 460){
            if(AltKey){
                KeyValue = 'H'; //dec to hex
            }
            else {
                KeyValue = '8';
            }
        }
        else if(KeyInput > 462 && KeyInput < 480){
            if(AltKey){
                KeyValue = 'A'; //radians to degrees
            }
            else {
                KeyValue = '7';
            }
        }
        else if(KeyInput > 502 && KeyInput < 520){
            if(AltKey){
                KeyValue = 'P'; //B key, percent x of y
            }
            else{
                KeyValue = '*'; //B key
            }
        }
        else if(KeyInput > 527 && KeyInput < 550){
            if(AltKey){
                KeyValue = 'Q'; //square root
            }
            else {
                KeyValue = '6';
            }
        }
        else if(KeyInput > 557 && KeyInput < 580){
            if(AltKey){
                KeyValue = 'I'; //dec to bin
            }
            else {
                KeyValue = '5';
            }
        }
        else if(KeyInput > 592 && KeyInput < 615){
            if(AltKey){
                KeyValue = 'G'; //degrees to radians
            }
            else {
                KeyValue = '4';
            }
        }
        else if(KeyInput > 657 && KeyInput < 690){
            if(AltKey){
                KeyValue = 'M'; //A key, modulus of division
            }
            else {
                KeyValue = '/'; //A key
            }
        }
        else if(KeyInput > 697 && KeyInput < 730){
            if(AltKey){
                KeyValue = 'T'; //tangent
            }
            else {
                KeyValue = '3';
            }
        }
        else if(KeyInput > 757 && KeyInput < 790){
            if(AltKey){
                KeyValue = 'C'; //cosine
            }
            else {
                KeyValue = '2';
            }
        }
        else if(KeyInput > 817 && KeyInput < 850){
            if(AltKey){
                KeyValue = 'S'; //sine
            }
            else {
                KeyValue = '1';
            }
        }
    }
    
    //debugging/graph
    //Serial.println(KeyInput);

}

//check and limit the length of floats stored as strings to avoid memory full crash
String MakeString(float FValue){
    String SValue = String(FValue, DEC_P);
    
    if(SValue.length() < 14){
        return SValue;
    }
    else {
        return "-000";
    }
}

//floating point modulus function
float ModFloat(float FValueY, float FValueX){
    float Result = 0.00;
    
    if(FValueX != 0.00){
        //y mod x, long form
        //floor rounds toward zero for positive and away from zero for negative
        //Quotient = floor(y/x)
        //Product = x * float(Quotient)
        //Modulus = y - Product
        
        //y mod x, short form
        //Result = y - (x * float(floor(y/x)))
        
        Result = FValueY - (FValueX * float(floor(FValueY / FValueX)));
    }
    
    return Result;
}

//(internal dependencies) 
//--global vars: InputMode, InputPrompt, KeyValue
//--functions: MakeString(), ModFloat()

//DoMath() has 3 operation groups and 2 modes:
//-Input operations: . (decimal), 0-9
//-Math operations: /, *, -, +, Negate, Power, Root
//-Stack operations: Drop/Backspace, Swap, Roll, Last Input
//--Input mode: when the input prompt is active
//--Stack mode: when the input prompt is not active
void DoMath(){ 
    //operand work variables
    String* InValueX;
    String* InValueY;
    
    //number of times to pop the stack during an operation
    //set by default below for input/stack mode two operand operations
    //modified in the switch cases for single operand operations
    int PopValue = 0; 
    
    //set work variables and defalut stack pops to use based on the mode
    //InValueX/Y will be used for math/stack operations, InputPrompt is used directly for 0-9 and . input
    //(input mode) use input and (stack 0) for operations
    //(stack mode) use (stack 0) and (stack 1) for operations
    if(InputMode){
        InValueX = &InputPrompt;
        InValueY = RpnStack.bottom();
        PopValue = 1;
    }
    else {
        InValueX = RpnStack.node(0);
        InValueY = RpnStack.node(1);
        PopValue = 2;
    }
    
    //does math or stack operations
    switch(KeyValue){
        //------Numbers------
        //(input mode) add number/decimal to input value
        //(stack mode) enter input mode, clear input prompt, add number/decimal to input value
        case '0': 
        case '1':
        case '2':
        case '3': //fall through since all input ops are treated similar
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case '.':
            //0-9 and . trigger input mode if your not already in input mode
            //reset input if your entering a new input mode session from stack mode
            if(!InputMode){
                InputPrompt = "";
                InputMode = true;
            }
            
            //check for, and block, double decimal input
            //input limiting to 14 chars/digits due to display/percision/memory constrants
            if(InputPrompt.length() < 14){ 
                if(KeyValue != '.'){
                    InputPrompt.concat(KeyValue); 
                }
                else if(InputPrompt.indexOf('.') < 0){
                    InputPrompt.concat(KeyValue);
                }
            }
            break;
        
        //------Enter------
        //(input mode) push input on stack, exit input mode
        //(stack mode) push duplicate stack 0
        case 'E':
            //convert to float then back to string adds 2 decimal places to all input values
            RpnStack.push(String(InValueX->toFloat(), DEC_P)); 
            
            //most math/stack operations cancel input mode and return to stack mode
            InputMode = false;
            break;
        
        //------Drop/Backspace------
        //(input mode) backspace 1 character, if input is empty return to stack mode
        //(stack mode) drop/pop (stack 0) value
        case 'D':
            if(InputMode){
                //backspace charater until input prompt is empty
                if(InputPrompt.length() > 0){
                    InputPrompt = InputPrompt.substring(0, InputPrompt.length()-1);
                }
                else {
                    //if input is empty, return to stack mode
                    InputMode = false;
                }
            }
            else {
                //if in stack mode, drop/pop stack 0
                RpnStack.pop();
            }
            break;
            
        //------Last Input------
        //(input mode) nothing
        //(stack mode) enter input mode and restore last input value to input prompt
        case 'L':
            if(!InputMode){
                InputMode = true;
            }
            break;
        
        //------Swap------
        //(input mode) swap input value and stack 0, exit input mode
        //(stack mode) swap stack 0 and stack 1
        case 'W':
            //two operand check
            if(InValueX && InValueY){
                //use String() if operands have been checked for length already (input and/or stack values)
                RpnStack.insert(String(InValueX->toFloat(), DEC_P), PopValue);
                RpnStack.insert(String(InValueY->toFloat(), DEC_P), PopValue);
                
                //binary operators pop 1/2 operand off the stack
                RpnStack.pop(PopValue);
                
                InputMode = false;
            }
            break;
            
        //------Roll------
        //(input mode) roll input prompt to top of stack, exits input mode
        //(stack mode) roll stack 0 to top of stack
        case 'R':
                
            //roll value of stack 0 or input prompt to top of stack
            RpnStack.insert(String(InValueX->toFloat(), DEC_P), RpnStack.length());
            
            //if rolling the stack, pop the rolled value from stack 0
            //if rolling input prompt, leave stack 0 alone, exit input mode
            if(!InputMode){
                RpnStack.pop();
            }
            else {
                InputMode = false;
            }
            break;
            
        //------Negation------    
        //(input mode) negate input value
        //(stack mode) negate stack 0 value
        case 'N':
            if(InValueX){
                RpnStack.insert(String(-InValueX->toFloat(), DEC_P), PopValue - 1);
                
                //unitary operators only need to pop 0/1 operand off the stack
                RpnStack.pop(PopValue - 1);
                
                InputMode = false;
            }
            break;
            
        //------Powers------
        //(input mode) raise (stack 0)^input, exit input mode
        //(stack mode) raise (stack 1)^(stack 0)
        case 'X':
            if(InValueX && InValueY){
                //use MakeString() to limit math result length by outputing error value which prevents crashes
                RpnStack.insert(MakeString(pow(InValueY->toFloat(), InValueX->toFloat())), PopValue);
                
                RpnStack.pop(PopValue);
                
                InputMode = false;
            }
            break;
        
        //------Roots------
        //(input mode) finds root x of input, exit input mode
        //(stack mode) finds root x of (stack 1)
        case 'Q':
            if(InValueX && InValueY){
                RpnStack.insert(MakeString(pow(InValueY->toFloat(), 1.00/InValueX->toFloat())), PopValue);
                
                RpnStack.pop(PopValue);
                
                InputMode = false;
            }
            break;
        
        //------Modulus------
        //(input mode) modulus of (stack 0)/input
        //(stack mode) modulus of (stack 1)/(stack 0)
        case 'M':
            
            if(InValueX && InValueY){
                //divide by zero check
                if(InValueX->toFloat() != 0.00){
                    RpnStack.insert(String(ModFloat(InValueY->toFloat(), InValueX->toFloat()), DEC_P), PopValue);
                    
                    RpnStack.pop(PopValue);
                    
                    InputMode = false;
                }
            }
            break;
            
        //------Division------
        //(input mode) divide (stack 0)/input
        //(stack mode) divide (stack 1)/(stack 0)
        case '/':
            if(InValueX && InValueY){
                if(InValueX->toFloat() != 0.00){
                    RpnStack.insert(MakeString(InValueY->toFloat()/InValueX->toFloat()), PopValue);
                    
                    RpnStack.pop(PopValue);
                    
                    InputMode = false;
                }
            }
            break;
        
        //------Multiplication------
        //(input mode) multiply (stack 0) * input
        //(stack mode) multiply (stack 1) * (stack 0)
        case '*':
            if(InValueX && InValueY){
                RpnStack.insert(MakeString(InValueY->toFloat() * InValueX->toFloat()), PopValue);
                
                RpnStack.pop(PopValue);
                
                InputMode = false;
            }
            break;
        
        //------Subtraction------
        //(input mode) subtract (stack 0) - input
        //(stack mode) subtract (stack 1) - (stack 0)
        case '-':
            if(InValueX && InValueY){
                RpnStack.insert(MakeString(InValueY->toFloat() - InValueX->toFloat()), PopValue);
                
                RpnStack.pop(PopValue);
                
                InputMode = false;
            }
            break;
        
        //------Addition------
        //(input mode) add (stack 0) + input
        //(stack mode) add (stack 1) + (stack 0)
        case '+':
            if(InValueX && InValueY){
                RpnStack.insert(MakeString(InValueY->toFloat() + InValueX->toFloat()), PopValue);
                
                RpnStack.pop(PopValue);
                
                InputMode = false;
            }
            break;
        
        //------Sine------
        //(input mode) sine of input
        //(stack mode) sine of stack 0
        case 'S':
            if(InValueX){
                //converts all degrees to radians for the built-in trig functions
                RpnStack.insert(String(sin(InValueX->toFloat() * DEG_TO_RAD), DEC_P + 3),PopValue - 1);
                
                RpnStack.pop(PopValue - 1);
                
                InputMode = false;
            }
            break;
            
        //------Cosine------
        //(input mode) cosine of input
        //(stack mode) cosine of stack 0
        case 'C':
            if(InValueX){
                RpnStack.insert(String(cos(InValueX->toFloat() * DEG_TO_RAD), DEC_P + 3),PopValue - 1);
                
                RpnStack.pop(PopValue - 1);
                
                InputMode = false;
            }
            break;
            
        //------Tangent------
        //(input mode) tanget of input
        //(stack mode) tanget of stack 0
        case 'T':
            if(InValueX){
                RpnStack.insert(String(tan(InValueX->toFloat() * DEG_TO_RAD), DEC_P + 3), PopValue - 1);
                
                RpnStack.pop(PopValue - 1);
                
                InputMode = false;
            }
            break;
        
        //------Binary------
        //(input mode) input to binary
        //(stack mode) stack 0 to binary
        case 'I':
            if(InValueX){
                RpnStack.insert(String(InValueX->toInt(), BIN), PopValue - 1);
                
                RpnStack.pop(PopValue - 1);
                
                InputMode = false;
            }
            break;
        
        //------Hexadecimal------
        //(input mode) input to hexadeciaml
        //(stack mode) stack 0 to hexadecimal
        case 'H':
            if(InValueX){
                RpnStack.insert(String(InValueX->toInt(), HEX), PopValue - 1);
                
                RpnStack.pop(PopValue - 1);
                
                InputMode = false;
            }
            break;
            
        //------Degrees to Radians------
        //(input mode) input to radians
        //(stack mode) stack 0 to radians
        case 'G':
            if(InValueX){
                RpnStack.insert(String(InValueX->toFloat() * DEG_TO_RAD, DEC_P), PopValue - 1);
                
                RpnStack.pop(PopValue - 1);
                
                InputMode = false;
            }
            break;
        
        //------Radians to Degrees------
        //(input mode) input to degrees
        //(stack mode) stack 0 to degrees
        case 'A':
            if(InValueX){
                RpnStack.insert(String(InValueX->toFloat() * RAD_TO_DEG, DEC_P), PopValue - 1);
                
                RpnStack.pop(PopValue - 1);
                
                InputMode = false;
            }
            break;
            
        //------Percent------
        //(input mode) input percentage of stack 0
        //(stack mode) stack 0 percentage of stack 1
        case 'P':
            if(InValueX && InValueY){
                RpnStack.insert(String(InValueY->toFloat() * (InValueX->toFloat() * 0.01), DEC_P), PopValue);
                
                RpnStack.pop(PopValue);
                
                InputMode = false;
            }
            break;
    }
}

//(internal dependencies) 
//--global vars: InputMode, InputPrompt KeyValue
void UpdateDisplay(){
    
    lcd.clear();
    
    //(input mode) print input on bottom line of display, then print stack 0/1/2
    //(stack mode) print stack 0/1/2/3 from bottom going up
    //stack value (i) counts up, display value (j) counts down 
    if(InputMode){
        //blinky cursor for input mode
        lcd.blink();
        
        //fill in stack values starting 1 line up from bottom (j=2)
        for(int i = 0, j = 2; i < 3; i++, j--){
            //print stack numbers on left side 
            lcd.setCursor(0,j);
            lcd.print(i);
            lcd.print(":");
            
            //empty stack/null pointer check
            if(RpnStack.node(i)){
                //right justify value, display stack node #i
                lcd.setCursor(20-RpnStack.node(i)->length(),j);
                lcd.print(*RpnStack.node(i));
            }
        }
        
        //right justify input, use bottom display line for input value 
        lcd.setCursor(19-InputPrompt.length(),3);
        lcd.print(InputPrompt);
    }
    else {
        //no blinky cursor for stack mode
        lcd.noBlink();
        
        //fill in stack values starting from bottom line (j=3)
        for(int i = 0, j = 3; i < 4; i++, j--){
            //print stack numbers on left side 
            lcd.setCursor(0,j);
            lcd.print(i);
            lcd.print(":");
            
            //empty stack/null pointer check
            if(RpnStack.node(i)){
                //right justify value, display stack node #i;
                lcd.setCursor(20-RpnStack.node(i)->length(),j);
                lcd.print(*RpnStack.node(i));
            }
        }  
    }
}


