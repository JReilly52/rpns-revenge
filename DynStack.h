/*Copyright 2019 J.P.Reilly
 *
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 */

#ifndef DYN_STACK
#define DYN_STACK


#include <Arduino.h>


//dynamic stack class using pointers and structures
class DynStack {
protected:
	//linked list structure for stack
	struct StackNode {
        //value of each stack node
		String Value;
        
        //points to next stack node
		StackNode *Next;
        
		//structure constructor
		StackNode(String Value, StackNode *Next = nullptr) {
			this->Value = Value;
			this->Next = Next;
		}
	};
    
    //points to bottom stack node
    StackNode *Bottom;
    int NodeCount;

public:
	DynStack();
	~DynStack();
	
    //create new node with value
	void push(String Value);
    
    //return pointer to nodenumber's value, added 4/18/19
    String* node(int NodeNumber) const; 
    
    //insert a node after stackpos, added 4/26/19
    void insert(String Value, int StackPos);
    
    //check if stack object has any nodes
	bool empty() const;
    
    //remove the lowest item from the stack
    //Modified 4/25/19, now allows removal of multiply items starting at lowest
    String pop(int Pops = 1);
    
    //returns pointer to bottom node's value
	String* bottom() const;
    
    //return length of stack, added 4/28/19
    int length() const; 
};

#endif // !DYN_STACK

